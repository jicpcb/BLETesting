//
//  ContentView.swift
//  BLETesting
//
//  Created by Jon on 8/31/21.
//

import SwiftUI
import CoreData

struct ContentView: View {
    
  
  //---------------------
  @ObservedObject var bleMan = BLEManager.shared
  //---------------------
  
    var body: some View {
      
      
      Button(action: {
        
        self.bleMan.startScanning()
        
      }) {
        SmallButton(btnText: "Scan", btnTextSize: 20, btnColor: (.black) , txtColor: (.white), width: 125, height: 40, radius: 15)
      }.preferredColorScheme(.light)

    }

}

private let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .medium
    return formatter
}()

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}

//---------------------------------------------
struct SmallButton: View {
  
  var btnText: String
  var btnTextSize: CGFloat
  var btnColor: Color? = Color.white
  var txtColor: Color = Color.black
  var width: CGFloat = 250
  var height: CGFloat = 60
  var radius: CGFloat = 30
  
  // font size 22 for large text
  // font size 18 for medium text
  
  var body: some View {
    ZStack {
      RoundedRectangle(cornerRadius: radius, style: .continuous)
        //.strokeBorder(Color(.darkGray), lineWidth: 2)
        .fill(btnColor ?? Color.gray)
        .frame(width: width, height: height, alignment: .bottomTrailing)
        
        
      Text("\(btnText)")
        .underline(false, color: nil)
        .frame(maxWidth: width, alignment:.center)
        .font(.custom("Helvetica Neue Regular", size: btnTextSize))
        .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
        .foregroundColor(txtColor)
    }
  }
}
