//
//  BLEManager.swift
//  CadioTesting
//
//  Created by Jon on 8/21.
//

import Foundation
import CoreBluetooth
import CoreData
import UserNotifications

//================================================
//------------------------------------------------
class BLEManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate, ObservableObject {
  
  let EMPTY_STRING: String = ""
  
  @Published var isSwitchedOn = false
  @Published var devices = [Device]()
  
  //  define a single shared instance for the app
  static let shared = BLEManager()
  
  // define a varabile for Apple's Bluetooth Manager
  private var centralMgr: CBCentralManager?
  
  
  // used in setSensorBehavior
  private let CMD_NONE: Int8 = 0
  private let CMD_SLEEP: Int8 = 1
  private let CMD_SET_LED: Int8 = 2
  private let CMD_SET_LED_ON: Int8 = 20
  private let CMD_SET_LED_OFF: Int8 = 25
  private let CMD_SET_CLOCK: Int8 = 3
  private let CMD_DELETE_SESSIONS: Int8 = 4
//  private let CMD_SLEEP_NOW
  
  //------------------------------------------------
  // MARK: public struct SENSOR_SERVICES
  public struct SENSOR_SERVICES {
    
    //  parent
    public static let BASE_SERVICE = CBUUID(string: "F3641400-00B0-4240-BA50-05CA45BF8ABC")
    
    //  streaming steps
    public static let STREAMING_STEPS = CBUUID(string: "F3641401-00B0-4240-BA50-05CA45BF8ABC")
    
    // clock service
    public static let SENSOR_CLOCK = CBUUID(string: "F3641402-00B0-4240-BA50-05CA45BF8ABC")
    
    //  event table
    public static let MOVEMENT_EVENT_TABLE = CBUUID(string: "F3641403-00B0-4240-BA50-05CA45BF8ABC")
  }
    
  //------------------------------------------------
  // MARK: public struct Movement used in Device
  public struct SnapMovement {
    var captured_date: Double = 0.0
    var computed_miles: Int64 = 0
    var computed_steps: Int64 = 0
    var data_source_id: Int16 = 0
    var elapsed_time: Double = 0.0
    var sensor_steps: Int64 = 0
    var sensor_uuid: String = ""
    var start_time: Double = 0.0
    var stop_time: Double = 0.0
  }
  
  //------------------------------------------------
  // MARK: public struct Device
  public struct Device: Identifiable {
    var id = UUID()
    var streamingSteps: Int64 = 0
    var recordCount: Int16 = 0
    var peripheral: CBPeripheral?
    var recordedMovements = [SnapMovement]()
    var connected: Bool {
      get {
        if peripheral?.state == .connected {
          return true
        } else {
          return false
        }
      }
    }
  }

  //------------------------------------------------
  // MARK: override init()
  override init() {
    
    super.init()
    centralMgr = CBCentralManager(delegate: self, queue: nil)
    centralMgr!.delegate = self
    
  }

  //================================================================================================
  // MARK: Start of helper functions
  //------------------------------------------------
  private func createMovement(snapUUID: String, _ record: (startTime: Int, stopTime: Int, steps: Int) ) -> SnapMovement {
    
    print("\nFUNCTION: \(#function)")
        
    var m_movement = SnapMovement()
    
    m_movement.captured_date = Date().timeIntervalSince1970
    m_movement.computed_miles = 0
    m_movement.computed_steps = 0
    m_movement.data_source_id = -1    // this is production code. -1 is a default value
    m_movement.elapsed_time = Double(record.stopTime - record.startTime)
    m_movement.sensor_steps = Int64(record.steps)
    m_movement.sensor_uuid = snapUUID
    m_movement.start_time = Double(record.startTime)
    m_movement.stop_time = Double(record.stopTime)
    
    return m_movement
  }
  
  
  //------------------------------------------------
  func disconnectAllPeripherals() {
    
    print("\nFUNCTION: \(#function)")
    
    for d in self.devices {
      centralMgr?.cancelPeripheralConnection(d.peripheral!)
    }
  }
  
  
  //------------------------------------------------
  public func getCmdData(from characteristic: CBCharacteristic) -> String {
  
    print("\nFUNCTION: \(#function), characteristic: \(characteristic.uuid)")
    
    let res: String = EMPTY_STRING
    
    guard let characteristicData = characteristic.value else {return res}
    
    let byteArray = [UInt8](characteristicData)
    
    let version = byteArray[0...1]
    print("version: \(version)")
    
    let hwVersion = byteArray[2]
    print("hwVersion: \(hwVersion)")
    
//    let stepsData = getSteps(bytes: byteArray[4...5])
    
    return res
  }
  
  
  //------------------------------------------------
  public func getStepData(from characteristic: CBCharacteristic) -> String {
    
    print("\nFUNCTION: \(#function), characteristic: \(characteristic.uuid)")
    
    var res: String = EMPTY_STRING
    
    guard let characteristicData = characteristic.value else {return res}
    
    let byteArray = [UInt8](characteristicData)

//    print(String(bytes: byteArray, encoding: .utf8) as Any)
    
//    var s:String = ""
//    var i:String = ""
    
//    for element in byteArray {
//      print("byte in byteArray: \(String(element))")
//      s += String(format: "%02X", element)
//      s += " "
//      i += padByte(string: String(element, radix: 2), toSize: 8)
//    }
//    print("characteristic converted from hex : \(s)")
//    print("characteristic converted to binary: \(i)")
    
    shiftStepBytes(bytes: byteArray)
    
    res = "\(characteristic)"
    return res
  }
  
  
  //------------------------------------------------
  func padByte(string : String, toSize: Int) -> String {
    
    var padded = string
    for _ in 0..<(toSize - string.count) {
      padded = "0" + padded
    }
    return padded
  }
  

  
  //================================================
  //------------------------------------------------
  private func setCharacteristicData(from characteristic: CBCharacteristic) -> Int {

    print("\nFUNCTION: \(#function): characteristic: \(characteristic)")
    
    guard let characteristicData = characteristic.value else { return -1 }

    var byteArray = [UInt8](characteristicData)
    
    if (byteArray[0] == 0) {
      byteArray[0] = UInt8("0")!
    }
    return Int(byteArray[0])
  }
  
  //================================================
  //  set the command byte in the payload.
  //  |  0  |  1   2   3   4
  //  | CMD | ...payload...
  //
  //  the type as defined on the board
  //  typedef enum { CMD_NONE = 0, CMD_SLEEP, CMD_SET_LED, CMD_SET_RTC } cmd_t;
  //  CMD_NONE (0) - does nothing
  //  CMD_SLEEP (1) - enter sleep state (immediately after BLE disconnection)
  //  CMD_SET_LED (2) - set led value according to payload first byte (0 means OFF, anything else means ON)
  //  CMD_SET_RTC (3) - set the internal RTC value (value is in payload)
  //
  //  convenience constants
  //  private let CMD_NONE: Int8 = 0
  //  private let CMD_SLEEP: Int8 = 1
  //  private let CMD_SET_LED: Int8 = 2
  //  private let CMD_SET_RTC: Int8 = 3
  //
  //  usage
  //  setSensorBehavior(p: peripheral, c: characteristic, cmd: CMD_SET_CLOCK)
  //  setSensorBehavior(p: peripheral, c: characteristic, cmd: CMD_SET_LED)
  //  setSensorBehavior(p: peripheral, c: characteristic, cmd: CMD_SLEEP)

  //------------------------------------------------
  func setSensorBehavior(sensor: CBPeripheral, characteristic: CBCharacteristic, cmd: Int8) -> Void {
    
    print("\nFUNCTION: \(#function): \n sensor: \(sensor.name ?? ""), \n characteristic: \(characteristic.uuid), \n cmd: \(cmd)")
    
    var cmdArray = [UInt8]()
    var bytesToSend = Data()
    var caseStr: String = ""
    
    switch cmd {
      case CMD_NONE:
        break

      case CMD_SLEEP:
        
        cmdArray = withUnsafeBytes(of: CMD_SLEEP.bigEndian, Array.init)
        print("case: CMD_SLEEP: \(cmdArray)")
        caseStr = "CMD_SLEEP"
        
      case CMD_SET_LED_ON:
        
        cmdArray = withUnsafeBytes(of: CMD_SET_LED.bigEndian, Array.init)

        let ledON = Int8(1) // this turns the LED on
        let ledArray = withUnsafeBytes(of: ledON.bigEndian, Array.init)
        
        cmdArray += ledArray
        
        caseStr = "CMD_SET_LED_ON"
        
        print("case: CMD_SET_LED_ON: \(cmdArray)")
      case CMD_SET_LED_OFF:
        
        cmdArray = withUnsafeBytes(of: CMD_SET_LED.bigEndian, Array.init)
        
        let ledOFF = Int8(0)  // this turns the LED off
        let ledArray = withUnsafeBytes(of: ledOFF.bigEndian, Array.init)
        
        cmdArray += ledArray

        caseStr = "CMD_SET_LED_OFF"
        
        print("case: CMD_SET_LED_OFF: \(cmdArray)")
        
      case CMD_SET_CLOCK: // Clock is really "RTC" (Real Time Clock) - a physical component on the board

        cmdArray = withUnsafeBytes(of: CMD_SET_CLOCK.bigEndian, Array.init)
        
        let sensorTimeEpochInt = Int32(Date().timeIntervalSince1970)
        let timeArray = withUnsafeBytes(of: sensorTimeEpochInt.bigEndian, Array.init)
        
        // concatenate the command to the front of hte time you are sening
        cmdArray += timeArray

        caseStr = "CMD_SET_CLOCK"
        
        print("\nINFO: case CMD_SET_CLOCK, setting sensor clock to: \(cmdArray)")
        
      case CMD_DELETE_SESSIONS:
//        cmdArray = withUnsafeBytes(of: CMD_DELETE_SESSIONS.bigEndian, Array.init)
        
//        caseStr = "CMD_DELETE_SESSIONS"
        print("case: CMD_DELETE_SESSIONS: \(cmdArray)")
        print("case: CMD_DELETE_SESSIONS not being executed.")
        
      default:
        caseStr = "DEFAULT"
        print("\nINFO: Switch default case in \(#function). You passed in a bogus cmd parameter.\n")
    }
    
    bytesToSend = Data(cmdArray)
    sensor.writeValue(bytesToSend, for: characteristic, type: .withResponse)
    
    print("\nINFO: Data has been written to \(characteristic.uuid) for case \(caseStr). \nLook in didWriteValueFor() for result or error.")
  }

  //================================================
  //   testing
  func shiftStepBytes(bytes: [UInt8]) -> Void {
    
    print("\nFUNCTION: \(#function): bytes: \(bytes)")
    
    var shiftValue = 0
    var steps: Int = 0
    let stepBytes = Array(bytes[3...4])
//    var batt: Int = 0
//    let battBytes = bytes[5]
    
    //- - - - - steps - - - - - - - - - - -
    for i in 0..<stepBytes.count {
      steps += Int(stepBytes[i]) << shiftValue
      shiftValue += 8
    }
    shiftValue = 0
    
    print("\nINFO: steps are: \(steps)")
    
    
    //- - - - - battery - - - - - - - - - - -
//    for j in 0..<battBytes {
//      batt += Int(battBytes) << shiftValue
//      shiftValue += 8
//    }
//    shiftValue = 0
//    print("battery is: \(batt)")
  }
  
  //================================================
  //  The inbound bytes look like this and are little endian.
  //  So you read them like this...
  //  [96, 171, 74, 249, 96, 226, 68, 163, 96, 21, 0, 0, 0]
  //  ------------------------------------------^--^--^--^ 4 step-count bytes
  //  ------------------------^----^---^----^ 4 stop-time bytes
  //  ------^---^----^---^ 4 start-time bytes
  //  -^  1 record count as an integer (no shift required)
  //------------------------------------------------
  func shiftBytes(bytes: [UInt8]) -> (startTime: Int, stopTime: Int, steps: Int) {
    
    print("\nFUNCTION: \(#function): bytes: \(bytes)")
    
    var shiftValue = 0
    var steps: Int = 0
    var start: Int = 0
    var stop: Int = 0
    
    let stepBytes = Array(bytes[9...12])
    let stopBytes = Array(bytes[5...8])
    let startBytes = Array(bytes[1...4])
    
    //- - - - - steps - - - - - - - - - - -
    for i in 0..<stepBytes.count {
      steps += Int(stepBytes[i]) << shiftValue
      shiftValue += 8
    }
    shiftValue = 0
    
    //- - - - - start - - - - - - - - - - -
    for j in 0..<startBytes.count {
      start += Int(startBytes[j]) << shiftValue
      shiftValue += 8
    }
    shiftValue = 0
    
    //- - - - - stop - - - - - - - - - - - -
    for k in 0..<stopBytes.count {
      stop += Int(stopBytes[k]) << shiftValue
      shiftValue += 8
    }
    
    return (start, stop, steps)
  }
  
  //------------------------------------------------

  func startScanning() {
    
    print("\nFUNCTION: \(#function)")
    
    var uuidArr = [CBUUID]()
    uuidArr.append(SENSOR_SERVICES.BASE_SERVICE)
    uuidArr.append(SENSOR_SERVICES.SENSOR_CLOCK)
    uuidArr.append(SENSOR_SERVICES.STREAMING_STEPS)
    uuidArr.append(SENSOR_SERVICES.MOVEMENT_EVENT_TABLE)

    centralMgr?.scanForPeripherals(withServices: uuidArr)
   }
  
  //------------------------------------------------
  
  func stopScanning() {
    print("\nFUNCTION: \(#function)")
    centralMgr?.stopScan()
  }
  
  
  
  //================================================================================================
  // MARK: Required Peripheral Manager functions
  //------------------------------------------------
  
  func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
    print("\nFUNCTION: \(#function): \n sensor: \(peripheral.name ?? ""), \n error: \(String(describing: error))")
    
    guard let services = peripheral.services else {return}
    
    print("\nINFO: \(peripheral.name ?? "") services are:")
    
    for service in services {
      print("\n  service: \(service.uuid)")
      peripheral.discoverCharacteristics(nil, for: service)
//      if service.characteristics != nil {
//          print("characteristic count is: ")
//      }
    
    }
  }

  //================================================
  //------------------------------------------------
  func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
    
    print("\nFUNCTION: \(#function): \n sensor: \(peripheral.name ?? ""), \n error: \(String(describing: error))")
    
    print("\nINFO: \(peripheral.name ?? "") characteristics are:")
    guard let characteristics = service.characteristics else { return }

    for characteristic in characteristics {
      print("\n  characteristic: \(characteristic.uuid)")
    }
    
    for characteristic in characteristics {
      
      switch characteristic.uuid {
        
        case SENSOR_SERVICES.BASE_SERVICE:
          print("base service found")
        
        case SENSOR_SERVICES.MOVEMENT_EVENT_TABLE:
          
          // new. written on 2021-05-25
          //  this readValue will initiate a callback to didUpdateValueFor()
          peripheral.readValue(for: characteristic)
          
        case SENSOR_SERVICES.STREAMING_STEPS:
          
          //  FEATURE:  subscribe to this when you want streaming steps
          peripheral.readValue(for: characteristic)
//          peripheral.setNotifyValue(true, for: characteristic)
//          getStepData(from: characteristic)
          // getCmdData()
          
        case SENSOR_SERVICES.SENSOR_CLOCK:
          
          setSensorBehavior(sensor: peripheral, characteristic: characteristic, cmd: CMD_SET_CLOCK)
//          peripheral.readValue(for: characteristic)
          

        default:
          print("Dont' have experience with this uuid: \(characteristic.uuid)")
      }
    }
  }
  
  //================================================
  //------------------------------------------------
  func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
    
    print("\nFUNCTION: \(#function) \n characteristic: \(characteristic.uuid), \n error: \(String(describing: error))")
        
  }
  
  
  //------------------------------------------------
  func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?) {
    print("\nFUNCTION: \(#function): sensor: \(peripheral.name ?? ""), error: \(String(describing: error))")
  }
  
  
  //------------------------------------------------
  func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?){
  
    print("\nFUNCTION: \(#function): \n sensor: \(peripheral.name ?? ""), \n characteristic \(characteristic.uuid), \n error: \(String(describing: error))")
    
    let idx = devices.firstIndex(where: {$0.peripheral == peripheral})
    
    guard characteristic.value != nil else { return }
    
    switch characteristic.uuid {

      case SENSOR_SERVICES.SENSOR_CLOCK:
        print("hi")


      case SENSOR_SERVICES.STREAMING_STEPS:
        
        let byteArray = [UInt8](characteristic.value!)
        print("\nINFO: updated Characteristic byteArray: \(byteArray)")
        
//        print(String(format: "%02X", byteArray[0]))
//        print(String(format: "%02X", byteArray[1]))
//        print(String(format: "%02X", byteArray[2]))
//        print(String(format: "%02X", byteArray[3]))
//        print(String(format: "%02X", byteArray[4]))
//        print(String(format: "%02X", byteArray[5]))

//        let d1 = byteArray[5]
//        let b1 = String(d1, radix: 2)
//        print("INFO: STREAMING_STEPS as bits are: \(b1) \n") // "10101"

        shiftStepBytes(bytes: byteArray)
        
        print("\nINFO: \(#function): \(devices[idx!].peripheral?.name ?? "") has logged: \(devices[idx!].streamingSteps) steps.")
        print("\nINFO: if step count is zero it may be that peripheral.setNotifyValue(true, for: characteristic) is commented out.")
        
      case SENSOR_SERVICES.MOVEMENT_EVENT_TABLE:
        
        //------- inbound record ---------------
        let byteArray = [UInt8](characteristic.value!)
        print("\nINFO: inbound byteArray: \(byteArray)")
        
        //  first byte in the array is the event table record count
        let recordCount = byteArray[0]
        print("\nINFO: recordCount: \(recordCount)")
        
        if recordCount == 0 {
          centralMgr?.cancelPeripheralConnection(peripheral)
          return
        }
        
        devices[idx!].recordCount = Int16(recordCount)
        
        
        //  break up the record and return the parts
        let record = shiftBytes(bytes: byteArray)
        print("\nINFO: the sensor record is: \(record)")
  
//        if recordCount == 0 {
        
//          if let clockCharacteristic = peripheral.services?.first?.characteristics?.filter({$0.uuid.uuidString.uppercased() == SENSOR_SERVICES.SENSOR_CLOCK.uuidString.uppercased()}) {

//            setSensorBehavior(p: peripheral, c: clockCharacteristic.first!, cmd: CMD_DELETE_SESSIONS)
//            devices[idx!].peripheral?.delegate = nil
//            centralMgr?.cancelPeripheralConnection(peripheral)
//          }
          
//        } else {

          if !peripheral.name!.isEmpty {
            if !(record.startTime < 1600000000 && record.stopTime < 1600000000) {

              let newMovement = createMovement(snapUUID: peripheral.identifier.uuidString, record)
              devices[idx!].recordedMovements.append(newMovement)
            }
          }
          
          // if last record in sessions table do this
          if recordCount == 1 {
            if let clockCharacteristic = peripheral.services?.first?.characteristics?.filter({$0.uuid.uuidString.uppercased() == SENSOR_SERVICES.SENSOR_CLOCK.uuidString.uppercased()}) {

              setSensorBehavior(sensor: peripheral, characteristic: clockCharacteristic.first!, cmd: CMD_DELETE_SESSIONS)
//              devices[idx!].peripheral?.delegate = nil
              centralMgr?.cancelPeripheralConnection(peripheral)
            }
          } else if recordCount > 1 {
            peripheral.readValue(for: characteristic)
          }
          
//        }

      default:
        print("found something we don't know anything about")
    }

   
  }

  
  //------------------------------------------------

  func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
    
    print("\nFUNCTION: \(#function): \n sensor: \(peripheral.name ?? ""), \n characteristic \(characteristic.uuid), \n error: \(String(describing: error))")
        
  }
  
  //------ End of Peripheral Functions -------------
  
  //================================================================================================
  // MARK: Required Central Manager functions

  //------------------------------------------------
  func centralManagerDidUpdateState(_ central: CBCentralManager) {
    
    print("\nFUNCTION: \(#function)")
    
     if central.state == .poweredOn {
      
      isSwitchedOn = true
      startScanning()
    } else {
      isSwitchedOn = false
    }
    print("\nINFO: \(#function), Bluetooth: \(isSwitchedOn ? "ON" : "OFF" )")
    print("INFO: Press the button on your sensor.")
    print("--------------------------------------\n")
    
  }
  //================================================
  //------------------------------------------------
  func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
    
    print("\nFUNCTION: \(#function): \n sensor: \(peripheral.name ?? ""), \n rssi: \(RSSI)")
    
//    print(" CBAdvertisementDataLocalNameKey:            \(advertisementData[CBAdvertisementDataLocalNameKey] ?? "EMPTY")\n")
//    print(" CBAdvertisementDataManufacturerDataKey:     \(advertisementData[CBAdvertisementDataManufacturerDataKey] ?? "EMPTY")\n")
//    print(" CBAdvertisementDataServiceDataKey:          \(advertisementData[CBAdvertisementDataServiceDataKey] ?? "EMPTY")\n")
//    print(" CBAdvertisementDataServiceUUIDsKey:         \(advertisementData[CBAdvertisementDataServiceUUIDsKey] ?? "EMPTY")\n")
//    print(" CBAdvertisementDataIsConnectable:           \(advertisementData[CBAdvertisementDataIsConnectable] ?? "EMPTY")\n")
//    print(" CBAdvertisementDataTxPowerLevelKey:         \(advertisementData[CBAdvertisementDataTxPowerLevelKey] ?? "EMPTY")\n")
//    print(" CBAdvertisementDataOverflowServiceUUIDsKey: \(advertisementData[CBAdvertisementDataOverflowServiceUUIDsKey] ?? "EMPTY")\n")
//    print(" CBAdvertisementDataSolicitedServiceUUIDsKey:\(advertisementData[CBAdvertisementDataSolicitedServiceUUIDsKey] ?? "EMPTY")\n")
    
    var aDevice = Device()
    aDevice.peripheral = peripheral
    devices.append(aDevice)
    
    if let deviceIdx = devices.firstIndex(where: {$0.peripheral == peripheral}) {
      
      devices[deviceIdx].peripheral?.delegate = self
      centralMgr?.connect(aDevice.peripheral!, options: nil)
      
    }
    stopScanning()
  }

  //================================================
  //------------------------------------------------
  func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
    
    print("\nFUNCTION: \(#function) \n failure on: \(peripheral.name ?? "") \n error: \(String(describing: error))")
    
    disconnectAllPeripherals()
  }
  
  //================================================
  //------------------------------------------------
  func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
    
    print("\nFUNCTION: \(#function): \n sensor: \(peripheral.name ?? "")\n")
    
    var pState: String = ""
    
    switch peripheral.state {
      case .connected:
        pState = "CONNECTED."
        
        if let deviceIdx = devices.firstIndex(where: {$0.peripheral == peripheral}) {
          devices[deviceIdx].peripheral?.discoverServices(nil)
        }

      case .connecting:
        pState = "CONNECT-ING."
      case .disconnected:
        pState = "DISCONNECTED."
      case .disconnecting:
        pState = "DISCONNECT-ING."
      @unknown default:
        print("error")
    }
    print("= = = = = = = = = = = = = = = = = = = = = = =")
    print("INFO: \(peripheral.name ?? "") is \(pState)")
    print("- - - - - - - - - - - - - - - - - - - - - - -\n")
    

  }
  //================================================
  //------------------------------------------------
  func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
    
    print("\nFUNCTION: \(#function): \n sensor: \(peripheral.name ?? ""), \n error: \(String(describing: error))")
    
    var pState: String = ""
    
    switch peripheral.state {
      case .connected:
        pState = "CONNECTED."
      case .connecting:
        pState = "CONNECT-ING."
      case .disconnected:
        pState = "DISCONNECTED."
      case .disconnecting:
        pState = "DISCONNECT-ING."
      @unknown default:
        print("error")
    }
    print("- - - - - - - - - - - - - - - - - - - - - - -")
    print("INFO: \(peripheral.name ?? "") is \(pState)")
    print("= = = = = = = = = = = = = = = = = = = = = = =\n")
  }
  
  //================================================
  //------------------------------------------------
  func centralManager(_ central: CBCentralManager, connectionEventDidOccur event: CBConnectionEvent, for peripheral: CBPeripheral) {

    print("\nFUNCTION: \(#function) sensor: \(peripheral.name ?? "")")
  }
  
  //------ End of Central Manager Func -------------
  //================================================================================================

  
  
}

/// Timer wrapper that automatically invalidates when released.
/// Read more: http://www.splinter.com.au/2019/03/28/timers-without-circular-references-with-pendulum
class Countdown {
    let timer: Timer
    
    init(seconds: TimeInterval, closure: @escaping () -> ()) {
        timer = Timer.scheduledTimer(withTimeInterval: seconds,
                repeats: false, block: { _ in
            closure()
        })
    }
    
    deinit {
        timer.invalidate()
    }
}
